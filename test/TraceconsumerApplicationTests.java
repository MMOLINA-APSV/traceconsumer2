@Test
    void testOrderUpdate() {
        // 1. send the message to be processed asynchronously
        Trace t = new Trace("truck-1569233700000", "test-truck", 1569233700000L, 38.42089633723801,    -1.4491918734674392);
        Message<Trace> m = new GenericMessage<Trace>(t);
        input.send(m);
        try {
       // asynchronous processing
            Thread.sleep(1000);
        // 2. Check that the asynchronous function correctly updated the order
            TransportationOrder result = repository.findById("test-truck").orElseThrow();
 
            assertEquals(result.getSt(), 0);
            assertEquals(result.getLastDate(), 1569233700000L);
            assertEquals(result.getLastLat(), 38.42089633723801);
            assertEquals(result.getLastLong(), -1.4491918734674392);
        } catch (NoSuchElementException e) {
            fail(); // the order should exist
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }